#ifndef _nfc_h_
#define _nfc_h_

#include <pcsclite.h>

#include <stdio.h>   /* Standard input/output definitions */
#include <string.h>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions */
#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */

/* bluez */
#include <sys/socket.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>
#include <bluetooth/rfcomm.h>
#include <bluetooth/l2cap.h>
#include <bluetooth/sdp.h>
#include <bluetooth/sdp_lib.h>

#include <pcsclite.h>
#include <ifdhandler.h>
#include <debuglog.h>

#ifdef __cplusplus
extern "C"
{
#endif

void closeSocket();
RESPONSECODE getDevice();
RESPONSECODE getServiceByName(char*);
RESPONSECODE getService(bdaddr_t);
RESPONSECODE sendData(PUCHAR, DWORD, PUCHAR, PDWORD, int);
void readUID(PDWORD, PUCHAR);
int readPresence();
int set_l2cap_mtu(int, uint16_t);

#ifdef __cplusplus
}
#endif

#endif
