#include "nfc.h"

#define CMD_BYTE 0x20; /* all cmnds start with this byte */
#define ATS_BYTE 0x21; /* cmd to return the ats */
#define PRS_BYTE 0x22; /* cmd to check for the presence of a icc */

char cmd[2]; /* commands */
int scan_sock; /* socket to scan for devices */
int com_sock; /* socket for communication */
fd_set socks; /* needed for select call to check for closed connection */
struct timeval timeout; /* needed for select */
inquiry_info *devices = NULL; /* holds found device */
int useL2CAP = 0;

//uint32_t svc_uuid_int32 = 0x1101; /* UUID of the service */
//uint32_t svc_uuid_int[] = {0x0100, 0, 0, 0x34fb};
uint32_t svc_uuid_int32 = 0x0100;
//uint16_t svc_uuid_int16 = 0x1101;

const char *svc_name = "SPPServer1";

/* clean up */
void closeSocket()
{
	if (com_sock > 0)
	{
		close(com_sock);
		com_sock = -1;
	}
	
	free(devices);
	if (scan_sock > 0)
	{
		close(scan_sock);
	}
}

/* scan for devices */
RESPONSECODE getDevice()
{
	RESPONSECODE rv;
	
	int max_rsp, num_rsp;
	int adapter_id, len, flags;
	int i;
	char addr[19] = { 0 };
	char name[248] = { 0 };
	
	adapter_id = hci_get_route(NULL);
	if (adapter_id < 0)
	{
		fprintf(stderr, "couldn't get adapter: error code %d: %s\n",
				errno, strerror(errno));
		return IFD_COMMUNICATION_ERROR;
	}
	
	scan_sock = hci_open_dev(adapter_id);
	if (scan_sock < 0)
	{
		Log1(PCSC_LOG_CRITICAL, "opening socket failed");
		return IFD_COMMUNICATION_ERROR;
	}
	
	len = 8;
	max_rsp = 255;
	flags = IREQ_CACHE_FLUSH;
	devices = (inquiry_info*) malloc(max_rsp * sizeof(inquiry_info));
	
	// scan
	num_rsp = hci_inquiry(adapter_id, len, max_rsp, NULL, &devices, flags);
	
	if (num_rsp < 0)
	{
		Log1(PCSC_LOG_CRITICAL, "hci_inquiry failed");
		return IFD_COMMUNICATION_ERROR;
	}
	
	if (num_rsp == 0)
	{
		Log1(PCSC_LOG_CRITICAL, "no devices found");
		return IFD_COMMUNICATION_ERROR;
	}
	
	// try to find the service on every device
	for (i = 0; i < num_rsp; ++i)
	{
		ba2str(&(devices + i)->bdaddr, addr);
		memset(name, 0, sizeof(name));
		if (0 != hci_read_remote_name(scan_sock, &(devices + i)->bdaddr,
									  sizeof(name), name, 0))
		{
			strcpy(name, "[unknown]");
		}
		printf("trying: %s %s\n", addr, name);
		
		// return first found
		if (getService(devices->bdaddr) == IFD_SUCCESS)
		{
			return IFD_SUCCESS;
		}
	}
	return IFD_COMMUNICATION_ERROR;
}

/* entry point for practical use with bt-address as string */
RESPONSECODE getServiceByName(char* strAddr)
{
	bdaddr_t target;
	str2ba(strAddr, &target);
	return getService(target);
}

/* connect to the service on the specified device */
RESPONSECODE getService(bdaddr_t target)
{	
	// service discovery
	int status_sdp;
	uuid_t svc_uuid;
	sdp_list_t *response_list, *search_list, *attrid_list;
	sdp_session_t *session = 0;
	uint32_t range = 0x0000ffff;
	uint16_t port = 0;
	
	// connect to the sdp server running on the remote machine
	session = sdp_connect(BDADDR_ANY, &target, 0);
	
	if (session == NULL)
	{
		fprintf(stderr, "couldn't get session: error code %d: %s\n",
				errno, strerror(errno));
		return IFD_COMMUNICATION_ERROR;
	}
	
	//sdp_uuid128_create(&svc_uuid, &svc_uuid_int);
	sdp_uuid32_create(&svc_uuid, svc_uuid_int32);
	search_list = sdp_list_append(0, &svc_uuid);
	attrid_list = sdp_list_append(0, &range);
	
	// get a list of service records that have UUID 0x1101
	response_list = NULL;
	status_sdp = sdp_service_search_attr_req(session, search_list,
				SDP_ATTR_REQ_RANGE, attrid_list, &response_list);
	
	if (status_sdp == 0)
	{
		sdp_list_t *proto_list = NULL;
		sdp_list_t *r = response_list;
		
		if (r == NULL)
		{
			Log1(PCSC_LOG_DEBUG, "no records matched");
		}
		
		// go through each of the service records
		for (; r; r = r->next)
		{
			sdp_record_t *rec = (sdp_record_t*) r->data;
			
			// get a list of the protocol sequences
			if (sdp_get_access_protos(rec, &proto_list) == 0)
			{
				sdp_data_t *d = sdp_data_get(rec, SDP_ATTR_SVCNAME_PRIMARY);
				if (d)
				{
					//printf("Service Name: %s\n", d->val.str);
				
					if (strncmp(d->val.str, svc_name, 10) == 0)
					{
						// get the RFCOMM port number
						port = sdp_get_proto_port(proto_list, RFCOMM_UUID);
						if (port == 0)
						{
							Log1(PCSC_LOG_DEBUG, "trying L2CAP");
							
							// get the L2CAP port number
							port = sdp_get_proto_port(proto_list, L2CAP_UUID);
							useL2CAP = 1;
						}
						sdp_list_free(proto_list, 0);
					}
				}
			}
			else
			{
				fprintf(stderr, "couldn't get access protocol: error code %d: %s\n",
					errno, strerror(errno));
			}
			sdp_record_free(rec);
		}
	}
	else
	{
		fprintf(stderr, "couldn't connect to sdp: error code %d: %s\n",
			errno, strerror(errno));
	}
	
	sdp_list_free(response_list, 0);
	sdp_list_free(search_list, 0);
	sdp_list_free(attrid_list, 0);
	sdp_close(session);
	
	if (port != 0)
	{
		// socket connect		
		int status = -1;
		
		if (useL2CAP < 1)
		{
			Log2(PCSC_LOG_DEBUG, "found service running on RFCOMM port %d", port);
			// allocate socket
			com_sock = socket(AF_BLUETOOTH, SOCK_STREAM, BTPROTO_RFCOMM);
			
			struct sockaddr_rc addrs = { 0 };
			
			// set the connection parameters (who to connect to)
			addrs.rc_family = AF_BLUETOOTH;
			addrs.rc_channel = port;
			addrs.rc_bdaddr = target;
			
			// connect to server
			status = connect(com_sock, (struct sockaddr *)&addrs, sizeof(addrs));
		}
		else
		{
			Log2(PCSC_LOG_DEBUG, "found service running on L2CAP psm %d", port);
			// allocate socket
			com_sock = socket(AF_BLUETOOTH, SOCK_SEQPACKET, BTPROTO_L2CAP);
			
			struct sockaddr_l2 addrs = { 0 };
			
			// set the connection parameters (who to connect to)
			addrs.l2_family = AF_BLUETOOTH;
			addrs.l2_psm = htobs(port);
			addrs.l2_bdaddr = target;
			
			// connect to server
			status = connect(com_sock, (struct sockaddr *)&addrs, sizeof(addrs));
			
			// set MTU
			set_l2cap_mtu(com_sock, 257);
		}

		// return success
		if (0 == status)
		{
			return IFD_SUCCESS;
		}
	}
	return IFD_COMMUNICATION_ERROR;
}

int set_l2cap_mtu(int sock, uint16_t mtu)
{
	struct l2cap_options opts;
	int optlen = sizeof(opts);
	int status = getsockopt(sock, SOL_L2CAP, L2CAP_OPTIONS, &opts, &optlen);
	
	if (status == 0)
	{
		opts.omtu = opts.imtu = mtu;
		status = setsockopt(sock, SOL_L2CAP, L2CAP_OPTIONS, &opts, optlen);
	}
	
	return status;
}

/* wrap data, transmit to the device and get answer */
RESPONSECODE sendData(PUCHAR TxBuffer, DWORD TxLength, 
				 PUCHAR RxBuffer, PDWORD RxLength, int wait)
{
	RESPONSECODE rv;
	if (com_sock < 0)
	{
		return IFD_COMMUNICATION_ERROR;
	}
	char apdu[TxLength + 2];
	int i, j, response_length, n = 0;
	int n2 = 0;
	
	// always use first 2 bytes byte as length of following data
	apdu[0] = TxLength >> 8 & 0xFF;
	apdu[1] = TxLength & 0xFF;
	
	for (i = 0; i < TxLength; ++i)
	{
		apdu[i + 2] = *(TxBuffer + i);
	}

	//// check if write is possible
	//FD_ZERO(&socks);
	//FD_SET(com_sock, &socks);
	////sock_max = s + 1;
	//n = select(com_sock + 1, (fd_set *) 0, &socks, (fd_set *) 0, &timeout);
	//if (n < 1)
	//{
	//	return IFD_COMMUNICATION_ERROR;
	//}

    send(com_sock, apdu, TxLength + 2, 0);

	Log2(PCSC_LOG_DEBUG, "wrote %d data bytes", TxLength);
	char buffer[65538];
	
	n = 0;
	if (wait > 0)
	{
		// use select to recognize broken connection
		timeout.tv_sec = 1;
		timeout.tv_usec = 0;
		FD_ZERO(&socks);
		FD_SET(com_sock, &socks);
		n = select(com_sock + 1, &socks, (fd_set *) 0, (fd_set *) 0, &timeout);
		
		if (n < 0)
		{
			return IFD_COMMUNICATION_ERROR;
		}
		else if (n == 0 || !FD_ISSET(com_sock, &socks))
		{
			*RxLength = 1;
			*RxBuffer = 0;
		    return IFD_SUCCESS;
		}
		else
		{
			n = recv(com_sock, buffer, sizeof(buffer), 0);
		}
	}
	else
	{
		n = recv(com_sock, buffer, sizeof(buffer), 0);
	}
	
	if (n > 0) 
	{
	    if (n == 1)
	    {
		    Log1(PCSC_LOG_DEBUG, "1 byte error response");
		    // handle errors
		    return IFD_COMMUNICATION_ERROR;
	    }
		
	    Log2(PCSC_LOG_DEBUG, "got %d response bytes", n);
	    // unwrap response
		response_length = (int) (0xFF & buffer[0]) << 8;
		response_length += (int) (0xFF & buffer[1]);
		
	    if (response_length != n - 2)
	    {
			*RxLength = response_length;
			
			i = 0;
			n2 = n;
			
			for (j = 2; j < n2; ++i)
			{
				*(RxBuffer + i) = buffer[j];
				++j;
			}
			
		    // try to read more
			while (n - 2 < response_length)
			{	
				n2 = 0;
				
				//Log1(PCSC_LOG_DEBUG, "rereceive");
				
				n2 = recv(com_sock, buffer, sizeof(buffer), 0);
				
				if (n2 > 0)
				{
					//Log2(PCSC_LOG_DEBUG, "got %d additional response bytes", n2);
					n += n2;
					
					for (j = 0; j < n2; ++i)
					{
						*(RxBuffer + i) = buffer[j];
						++j;
					}
				}
				else
				{
					Log3(PCSC_LOG_ERROR, "response length wrong, is: %d  should be: %d",
						n - 2, response_length);
		    		return IFD_COMMUNICATION_ERROR;
				}
			}
			
		    //Log3(PCSC_LOG_DEBUG, "concated response length is: %d  should be: %d",
			//			i, response_length);
			
			rv = IFD_SUCCESS;
	    }
	    else
	    {
		    *RxLength = response_length;
		    for (i = 0; i < response_length; ++i)
		    {
			    *(RxBuffer + i) = buffer[2 + i];
		    }
		    //Log_Xxd(PCSC_LOG_INFO, "RAPDU: ", *RxBuffer, *RxLength);
		    rv = IFD_SUCCESS;
	    }
	}
	else
	{
	    Log1(PCSC_LOG_ERROR,
			 "no response, meaning eof, reader not usable anymore\n");
		closeSocket();
	    rv = IFD_COMMUNICATION_ERROR;
	}
	return rv;
}

/* get the uid from the device and return it as an atr */
void readUID(PDWORD Length, PUCHAR Value)
{
	cmd[0] = CMD_BYTE;
	cmd[1] = ATS_BYTE;
	DWORD rxLength;
	int i ,j;
	char atr[MAX_ATR_SIZE - 4];
	
	// send the ats cmd
	if (sendData(&cmd, sizeof(cmd), &atr, &rxLength, 0) == IFD_SUCCESS)
	{
		// construct standart contactless ATR
		*(Value + 0) = 0x3B; //TS direct convention
		*(Value + 1) = 0x88; //T0 TD1 available, 8 historical bytes
		*(Value + 2) = 0x80; //TD1 TD2 follows, protocol T0
		*(Value + 3) = 0x01; //TD2 no Tx3, protocol T1
		j = 4;
		char crc = 0x88 ^ 0x80 ^ 0x01;
		for (i = 0; i < rxLength; ++i)
		{
			*(Value + i + j) = atr[i];
			crc ^= atr[i];
		}
		*(Value + i + j) = crc;
		*Length = rxLength + j + 1;
	}
	else
	{
		*Length = 0;
	}
}

/* answer to the pcscd polling */
int readPresence()
{
	// construct presence cmd
	cmd[0] = CMD_BYTE;
	cmd[1] = PRS_BYTE;
    DWORD rxLength = 0;
	char response[1];
	
	// send cmd
	if (sendData(&cmd, sizeof(cmd), &response, &rxLength, 1) == IFD_SUCCESS)
	{
		// if icc present the response will be just 0x1
		if (rxLength == 1)
		{
			return (int) response[0];
		}
	}
	return 0;
}
